:- module(zip, [zip/2]).

zip(List, Result) :-
    maplist(length,List,Result).

zip_step([], []).
zip_step(List, Result) :- 
    firsts(List, Firsts, Rests),
    print(Firsts),
    nl,
    print(Rests),
    nl,
    zip_step(Rests, [Result | Firsts]).


firsts([], [], []).
firsts([FirstList|RestLists], Firsts, Rests) :-
    FirstList = [First | Rest],
    firsts(RestLists, RestFirsts, RestRests),
    Firsts = [First | RestFirsts],
    Rests = [Rest | RestRests].

:- begin_tests(zip).

test(firsts) :-
    firsts([[1,2,3], [4,5,6],[7,8,9]], Firsts, Rests).
    
test(zip_step) :-
    zip_step([[1,2,3], [4,5,6],[7,8,9]], Result).

test(zip) :-
    zip([[1,2,3], [4,5,6]], Pairs),
    print(Pairs).

:- end_tests(zip).
