:- module(slice, [slice/2]).

slice([], []).
slice([_], []).
slice(Str, Pairs) :-
    Str = [First|SecondRest],
    SecondRest = [Second | _],
    
    Head = [First, Second],

    slice(SecondRest, RestPairs),
    append([Head], RestPairs, Pairs).

:- begin_tests(slice).

:- use_module("counts.P").
:- use_module("slice.P").

test(merge) :-
    atom_chars("AFDSJKLFDSJKLFSJLKFDKSD", Chars),
            slice(Chars, Pairs),
            print(Pairs).

:- end_tests(merge).
